/**
 * @ngdoc overview
 * @name transcend.resource
 * @description
 # Resource Module
 The "Resource" module provides a set of utility services to make working with an {@link https://docs.angularjs.org/api/ngResource/service/$resource AngularJS resource},
 or list of resources, easier. In addition it offers a number of "form" related components to assist in the Create, Read,
 Update, and Delete (CRUD) operations to make these actions generic enough to be used in common situations. One such
 control is the {@link dynamicForm Dynamic Form Directive} which will automatically create a form based on an object
 structure - and/or meta-data for that object.

 ## Installation
 The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
 commands to install this package:

 ```
 bower install https://bitbucket.org/transcend/bower-resource.git
 ```

 Other options:

 *   * Download the code at [http://code.tsstools.com/bower-resource/get/master.zip](http://code.tsstools.com/bower-resource/get/master.zip)
 *   * View the repository at [http://code.tsstools.com/bower-resource](http://code.tsstools.com/bower-resource)
 *   * If you have permissions, view the source code at [http://code.tsstools.com/web-common](http://code.tsstools.com/web-common/src/ee541a3bce850ae77f6d7948a00cc352b4f67b21/modules/resource/?at=develop)

 When you have pulled down the code and included the files (and dependency files), simply add the module as a
 dependency to your application:

 ```
 angular.module('myModule', ['transcend.resource']);
 ```
 ## To Do
 - Add additional unit tests
 - Finish documentation
*/
/**
 * @ngdoc service
 * @name transcend.resource.$resourceController
 *
 * @description
 * A controller factory that will provide a new function to be used as a controller in a directive. This controller
 * provides all of the basic functionality to manipulate, track changes, and sync a resource.
 *
 <pre>
 angular.module('myModule')
 .directive('myDirective', function ($resourceController, MyResource) {
     'use strict';

      return {
        scope: {
          onSaveBegin: '&', // Automatically called by the controller.
          onSave: '&',      // Automatically called by the controller.
          onSaveError: '&'  // Automatically called by the controller.
        },
        template: "...",

        // Call the $resourceController factory to
        // create a new controller for this directive.
        controller: $resourceController({
            dirtyCheckingEnabled: true,
            resource: MyResource.get({ id: 123 })
          }),

        link: function (scope, el, attr, controller) {
          // The "controller" is the instance created
          // from the $resourceController above.
        }
     };
 });
 </pre>
 *
 * @requires $scope
 * @requires transcend.core.$object
 *
 * @param {Object} config The configuration object that is used to setup the controller. See the {@link transcend.resource.$resourceController#methods_extend extend method}
 * "conf" argument for a detailed list of acceptable configuration properties (they are the same).
 */
/**
             * @ngdoc method
             * @name transcend.resource.$resourceController#dirty
             * @methodOf transcend.resource.$resourceController
             *
             * @description
             * Function to determine if the state of the resource is dirty or not. The dirty check takes into account
             * the resource's state as well as the form's validity if there is a form set for this controller. This is
             * done to prevent the user from being able to save a resource that has invalid property values.
             *
             * @return {Boolean} True if the resource is dirty.
             * @example
             <pre>
             // Fake a form with an invalid state.
             controller.form = { $invalid : true };
             var isDirty = controller.dirty();
             expect(isDirty).toBeTruthy();
             </pre>
             */
/**
             * @ngdoc method
             * @name transcend.resource.$resourceController#save
             * @methodOf transcend.resource.$resourceController
             *
             * @description
             * Saves the resource's changes. This will call the "$save" function on the AngularJS resource. See the
             * {@link https://docs.angularjs.org/api/ngResource/service/$resource $resource} documentation for more
             * information on the built in resource methods.
             *
             * @example
             <pre>
             controller.save();
             expect(controller.resource.$save).toHaveBeenCalled();
             </pre>
             */
/**
             * @ngdoc method
             * @name transcend.resource.$resourceController#reset
             * @methodOf transcend.resource.$resourceController
             *
             * @description
             * Resets any changes to the resource back to its' original state.
             *
             * @example
             <pre>
             controller.resource.value = 456;
             controller.reset();
             expect(controller.resource.value).toEqual(123);
             </pre>
             */
/**
             * @ngdoc method
             * @name transcend.resource.$resourceController#extend
             * @methodOf transcend.resource.$resourceController
             *
             * @description
             * Extends the controller with additional configurations/properties.
             *
             * @param {Object} conf The configuration object to extend the controller instance with. The acceptable
             * configurations properties to pass are as follows:
             * #### Valid Properties:
             *   * __Resource__ - The resource service to use to retrieve the single resource.
             *   * __resource__ - An already retrieved/instantiated resource instance.
             *   * __eventPrefix__ - An prefix to add to the events that are fired when actions are taken on this resource.
             *   * __form__ - A reference to the view's form element which will be used to check teh forms valid state.
             *     If no form is passed in than the form validity check will be skipped.
             *   * __dirtyCheckingEnabled__ - A flag that determines if we should keep track of changes to the resource
             *     or not. This is useful to enhance performance if the specific usage does not care about the "dirty"
             *     state of the resource. If "dirtyCheckingEnabled" is set to false than the resource controller will
             *     not have to create a copy/clone of the resource object to check against later.
             *
             * @example
             <pre>
             controller.extend({
                dirtyCheckingEnabled: true,
                form: scope.form,
                eventPrefix: 'resources',
                Resource: MyResource
              });
             </pre>
             */
/**
 * @ngdoc service
 * @name transcend.resource.$metaData
 *
 * @description
 * The "$metaData" factory provides the ability to create/store information that defines the structure of a given object.
 * This information can be passed to the {@link transcend.resource.directive:dynamicForm dynamicForm directive} to have it create
 * the form with specific information.
 *
 <pre>
 var metaData = $metaData({ name: 'rusty', age: 28 });

 expect(metaData.length).toEqual(2);
 expect(metaData[0].placeholder).toEqual('Name');
 expect(metaData[0].tooltip).toEqual('Name');
 expect(metaData[0].type).toEqual('text');
 expect(metaData[0].property).toBe(original.name);
 expect(metaData[0].key).toEqual('name');
 expect(metaData[0].label).toEqual('Name');
 </pre>
 *
 * @requires $q
 * @requires $timeout
 *
 * @param {Object} resource Resource or object to generate the meta-data from.
 */
/**
 * @ngdoc directive
 * @name transcend.resource.directive:dynamicForm
 *
 * @description
 * The 'dynamicForm' directive provides a way to create forms automatically based on an object's properties and value.
 * An optional "meta-data" configuration can be passed in to provide additional details/restrictions on the properties
 * - such as default values, validations, and types.
 *
 * @restrict EA
 * @element ANY
 *
 * @param {Object} resource The resource/object to build the form from.
 * @param {Object=} metaData The meta-data to use/bind to build the form.
 * @param {Object=} controller An optional api that will be used to let the declarer of the directive gain access to the
 * controller of the directive (the basic API of the controller).
 * @param {expression=} onSaveBegin {@link https://docs.angularjs.org/guide/expression Expression} to evaluate upon the
 * save process starting.
 * @param {expression=} onSave {@link https://docs.angularjs.org/guide/expression Expression} to evaluate upon the save
 * process successfully completing.
 * @param {expression=} onSaveError {@link https://docs.angularjs.org/guide/expression Expression} to evaluate upon the save
 * process failing.
 *
 * @requires ngModel
 *
 * @example
 <example module="app">
 <file name="index.html">
 <div ng-controller="Ctrl">

 <dynamic-form resource="resource"></dynamic-form>
 <hr/>
 <strong>Raw Model:</strong><br/>
 {{resource}}

 </div>
 </file>
 <file name="script.js">
 angular.module('app', ['transcend.resource'])
 .controller('Ctrl', function($scope) {
        $scope.resource =  {
          firstName: 'Rusty',
          lastName: 'Green',
          email: 'rgreen@tssgis.com',
          address: {
            street: '123 Elm Street',
            city: 'Happy Town',
            zipCode: 12345
          }
        };
      });
 </file>
 </example>
 */
/**
 * @ngdoc directive
 * @name transcend.resource.directive:formComponent
 *
 * @description
 * The 'dynamicForm' directive provides a way to create forms automatically based on an object's properties and value.
 * An optional "meta-data" configuration can be passed in to provide additional details/restrictions on the properties
 * - such as default values, validations, and types.
 *
 * @restrict EA
 * @element ANY
 *
 * @param {Object} resource The resource/object to build the form from.
 * @param {Object=} metaData The meta-data to use/bind to build the form.
 * @param {Object=} controller An optional api that will be used to let the declarer of the directive gain access to the
 * controller of the directive (the basic API of the controller).
 * @param {expression=} onSaveBegin {@link https://docs.angularjs.org/guide/expression Expression} to evaluate upon the
 * save process starting.
 * @param {expression=} onSave {@link https://docs.angularjs.org/guide/expression Expression} to evaluate upon the save
 * process successfully completing.
 * @param {expression=} onSaveError {@link https://docs.angularjs.org/guide/expression Expression} to evaluate upon the save
 * process failing.
 *
 * @requires ngModel
 */
/**
 * @ngdoc directive
 * @name transcend.resource.directive:preChange
 *
 * @description
 * The 'preChange' directive provides a way to fire a function when the ng-model value is changed but it will fire
 * before the actual value has changed - unlike the "ng-change" directive which fires after the model is updated.
 *
 * @restrict A
 * @element ANY
 *
 * @param {expression} preChange {@link https://docs.angularjs.org/guide/expression Expression} to evaluate upon change
 * in input value.
 *
 * @requires ngModel
 */
/**
 * @ngdoc service
 * @name transcend.resource.$resourceListController
 *
 * @description
 * A controller factory that will provide a new function to be used as a controller in a directive that has a list of
 * resources. This controller provides all of the basic functionality to manipulate, track changes, and sync a list
 * of {@link https://docs.angularjs.org/api/ngResource/service/$resource AngularJS resources}.
 *
 <pre>
 angular.module('myModule')
 .directive('myListDirective', function ($resourceListController, MyResource) {
     'use strict';

      return {
        scope: {
          onSaveBegin: '&', // Automatically called by the controller.
          onSave: '&',      // Automatically called by the controller.
          onSaveError: '&'  // Automatically called by the controller.
        },
        template: "...",

        // Call the $resourceListController factory to
        // create a new controller for this directive.
        controller: $resourceListController({
            dirtyCheckingEnabled: true,
            listProperty: 'myResourcesList',
            Resource: MyResource // Will internally call MyResource.query()
          }),

        link: function (scope, el, attr, controller) {
          // The "controller" is the instance created
          // from the $resourceListController above.
          // Note: "controller.myResourcesList" is the
          // result of "MyResource.query()".
        }
     };
 });
 </pre>
 *
 * @requires $scope
 * @requires transcend.resource.$resourceList
 *
 * @param {Object} config The configuration object that is used to setup the controller. See the {@link transcend.resource.$resourceListController#methods_extend extend method}
 * "conf" argument for a detailed list of acceptable configuration properties (they are the same).
 */
/**
             * @ngdoc method
             * @name transcend.resource.$resourceListController#dirty
             * @methodOf transcend.resource.$resourceListController
             *
             * @description
             * Function to determine if any of the resources in the list or resources has a "dirty" state or not. The
             * dirty check takes into account the resource's state as well as the form's validity if there is a form
             * set for this controller. This is done to prevent the user from being able to save a resource that has
             * invalid property values.
             *
             * @return {Boolean} True if the resource is dirty.
             * @example
             <pre>
             // Fake a form with an invalid state.
             controller.form = { $invalid : true };
             var isDirty = controller.dirty();
             expect(isDirty).toBeTruthy();
             </pre>
             */
/**
             * @ngdoc method
             * @name transcend.resource.$resourceListController#save
             * @methodOf transcend.resource.$resourceListController
             *
             * @description
             * Saves all dirty resource's changes . This will call the "$save" function on each of the AngularJS
             * resources in the list. See the {@link https://docs.angularjs.org/api/ngResource/service/$resource $resource}
             * documentation for more information on the built in resource methods.
             *
             * @example
             <pre>
             controller.save();
             expect(controller.myResourceList[4].$save).toHaveBeenCalled();
             </pre>
             */
/**
             * @ngdoc method
             * @name transcend.resource.$resourceListController#reload
             * @methodOf transcend.resource.$resourceListController
             *
             * @description
             * Resets any changes to the existing resources and re-queries the resource list to get a new list of
             * resources.
             *
             * @example
             <pre>
             controller.reload();
             expect(controller.MyResource.query).toHaveBeenCalled();
             expect(controller.dirty()).toBeFalsy();
             </pre>
             */
/**
             * @ngdoc method
             * @name transcend.resource.$resourceListController#reset
             * @methodOf transcend.resource.$resourceListController
             *
             * @description
             * Resets any resources in the list that are dirty back to their original state.
             *
             * @example
             <pre>
             controller.resourcesList[4].value = 456;
             controller.reset();
             expect(controller.resourcesList[4].value).toEqual(123);
             </pre>
             */
/**
             * @ngdoc method
             * @name transcend.resource.$resourceListController#add
             * @methodOf transcend.resource.$resourceListController
             *
             * @description
             * Adds a new resource to the list of resources. Note, this is the same functionality as
             * calling the "push" method.
             *
             * @example
             <pre>
             controller.resourcesList.add({ name: 'My New Resource' });
             </pre>
             */
/**
             * @ngdoc method
             * @name transcend.resource.$resourceListController#update
             * @methodOf transcend.resource.$resourceListController
             *
             * @description
             * Tells the list that a resource has been updated. This will trigger the resource list to copy/clone the
             * original resource value and then start tracking changes. __Note__, this method must be called prior to
             * the actual object being updated otherwise the cached "original" value will not be correct. You must use
             * the "pre-change" directive to call this method in a view to get the resource reference before the ng-model
             * value changes.
             *
             * See the {@link preChange preChange directive} for an example.
             *
             * @example
             <pre>
             var item = controller.resources[4];
             controller.update(item);
             item.value = "456";
             </pre>
             */
/**
             * @ngdoc method
             * @name transcend.resource.$resourceListController#remove
             * @methodOf transcend.resource.$resourceListController
             *
             * @description
             * Removes a resource from the list of resources. When the controller's {@link transcend.resource.$resourceListController#save save}
             * method is called, the resource that was removed from the list will have its "$delete" method called.
             *
             * @example
             <pre>
             expect(controller.list.length).toEqual(5);
             controller.remove(controller.list[2]);
             expect(controller.list.length).toEqual(4);
             </pre>
             */
/**
             * @ngdoc method
             * @name transcend.resource.$resourceListController#extend
             * @methodOf transcend.resource.$resourceListController
             *
             * @description
             * Extends the controller with additional configurations/properties.
             *
             * @param {Object} conf The configuration object to extend the controller instance with. The acceptable
             * configurations properties to pass are as follows:
             * #### Valid Properties:
             *   * __Resource__ - The resource service to use to query for the list of resources.
             *   * __resource__ - An already retrieved/instantiated resource list/array instance.
             *   * __listProperty__ - The name (property name) of the list that will be appended to the $scope of the
             *   controller. By default this is "$scope.resources".
             *   * __eventPrefix__ - An prefix to add to the events that are fired when actions are taken on this resource.
             *   * __form__ - A reference to the view's form element which will be used to check teh forms valid state.
             *     If no form is passed in than the form validity check will be skipped.
             *   * __dirtyCheckingEnabled__ - A flag that determines if we should keep track of changes to the resource
             *     list or not. This is useful to enhance performance if the specific usage does not care about the
             *     "dirty" state of the resources in the list. If "dirtyCheckingEnabled" is set to false than the
             *     resource list controller will not have to create a copy/clone of the changed resource object to
             *     check against later.
             *
             * @example
             <pre>
             controller.extend({
                listProperty: 'myResourceList',
                dirtyCheckingEnabled: true,
                form: scope.form,
                eventPrefix: 'resources',
                Resource: MyResource
              });
             </pre>
             */
/**
 * @ngdoc service
 * @name transcend.resource.$resourceList
 *
 * @description
 * The "resourceList" factory provides the ability to extend an AngularJS resource result (if it is an array) in order
 * to provide basic functionality to manipulate, track changes, and sync a list of {@link https://docs.angularjs.org/api/ngResource/service/$resource AngularJS resources}.
 *
 <pre>
 $scope.list = $resourceList(MyResource.query());
 // Or...
 $scope.list = $resourceList(MyResource); // Will call MyResource.query() internally.
 </pre>
 *
 * @requires $q
 * @requires $timeout
 *
 * @param {ng.$resource} resource An instantiated resource list (from Resource.query() call) or a Resource service. In the
 * case of the latter, the list will internally call Resource.query().
 * @param {Object} options Options to configure the behaviour of the instance. Valid options include:
 * #### Options
 * * __params__ - Parameter object to pass when calling "Resource.query(options.params)".
 * * __autoSync__ - Flag that determines if the list should automatically save resource changes when they happen or wait
 * till the "$sync method" is called (false by default).
 */
/**
           * @ngdoc property
           * @name transcend.resource.$resourceList#state
           * @propertyOf transcend.resource.$resourceList
           *
           * @description
           * Provides the current state of the resource list. Options include; 'clean' or 'dirt'.
           *
           * @example
           <pre>
           var list = $resourceList(...);
           expect(list.state).toEqual('clean');
           list.$update(...);
           expect(list.state).toEqual('dirty');
           </pre>
           */
/**
           * @ngdoc property
           * @name transcend.resource.$resourceList#clean
           * @propertyOf transcend.resource.$resourceList
           *
           * @description
           * A flag that states whether the resource is in a clean state.
           *
           * @example
           <pre>
           var list = $resourceList(...);
           expect(list.clean).toEqual(true);
           list.$update(...);
           expect(list.clean).toEqual(false);
           </pre>
           */
/**
           * @ngdoc property
           * @name transcend.resource.$resourceList#dirty
           * @propertyOf transcend.resource.$resourceList
           *
           * @description
           * A flag that states whether the resource is in a dirty state.
           *
           * @example
           <pre>
           var list = $resourceList(...);
           expect(list.dirty).toEqual(false);
           list.$update(...);
           expect(list.dirty).toEqual(true);
           </pre>
           */
/**
           * @ngdoc property
           * @name transcend.resource.$resourceList#changes
           * @propertyOf transcend.resource.$resourceList
           *
           * @description
           * An object that holds the list of changed (dirty) resources.
           *
           * @example
           <pre>
           var list = $resourceList(...);
           expect(list.changes.adds).toEqual([...]);
           expect(list.changes.updates).toEqual([...]);
           expect(list.changes.removes).toEqual([...]);
           expect(list.changes.all()).toEqual([...]);
           </pre>
           */
/**
           * @ngdoc method
           * @name transcend.resource.$resourceList#$add
           * @methodOf transcend.resource.$resourceList
           *
           * @description
           * Adds a new resource to the list of resources. Note, this is the same functionality as
           * calling the "push" method.
           *
           * @example
           <pre>
           var list = $resourceList(...);
           list.$add({ name: 'My New Resource' });
           </pre>
           */
/**
           * @ngdoc method
           * @name transcend.resource.$resourceList#$update
           * @methodOf transcend.resource.$resourceList
           *
           * @description
           * Tells the list that a resource has been updated. This will trigger the resource list to copy/clone the
           * original resource value and then start tracking changes. __Note__, this method must be called prior to
           * the actual object being updated otherwise the cached "original" value will not be correct. You must use
           * the "pre-change" directive to call this method in a view to get the resource reference before the ng-model
           * value changes.
           *
           * See the {@link preChange preChange directive} for an example.
           *
           * @example
           <pre>
           var item = resourceList[4];
           resourceList.$update(item);
           item.value = "456";
           </pre>
           */
/**
           * @ngdoc method
           * @name transcend.resource.$resourceList#$remove
           * @methodOf transcend.resource.$resourceList
           *
           * @description
           * Removes a resource from the list of resources. When the list's {@link transcend.resource.$resourceList#$sync $sync}
           * method is called, the resource that was removed from the list will have its "$delete" method called.
           *
           * @example
           <pre>
           expect(list.length).toEqual(5);
           controller.$remove(list[2]);
           expect(list.length).toEqual(4);
           </pre>
           */
/**
           * @ngdoc method
           * @name transcend.resource.$resourceList#reload
           * @methodOf transcend.resource.$resourceList
           *
           * @description
           * Resets any changes to the existing resources and re-queries the resource list to get a new list of
           * resources.
           *
           * @example
           <pre>
           list.reload();
           expect(list.dirty()).toBeFalsy();
           </pre>
           */
/**
           * @ngdoc method
           * @name transcend.resource.$resourceList#reset
           * @methodOf transcend.resource.$resourceList
           *
           * @description
           * Resets any resources in the list that are dirty back to their original state.
           *
           * @example
           <pre>
           resourcesList[4].value = 456;
           resourcesList.reset();
           expect(resourcesList[4].value).toEqual(123);
           </pre>
           */
/**
           * @ngdoc method
           * @name transcend.resource.$resourceList#sync
           * @methodOf transcend.resource.$resourceList
           *
           * @description
           * Saves/syncs all dirty resource's changes . This will call the appropriate built in resource method on each
           * of the AngularJS resources in the list to persist the changes ($save, $delete, or create). See the
           * {@link https://docs.angularjs.org/api/ngResource/service/$resource $resource} documentation for more
           * information on the built in resource methods.
           *
           * @example
           <pre>
           myResourceList.$remove(myResourceList[4]);
           myResourceList.sync();
           expect(myResourceList[4].$delete).toHaveBeenCalled();
           </pre>
           */
