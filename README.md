# Resource Module
The "Resource" module provides a set of utility services to make working with an [AngularJS resource](https://docs.angularjs.org/api/ngResource/service/$resource),
or list of resources, easier. In addition it offers a number of "form" related components to assist in the Create, Read,
Update, and Delete (CRUD) operations to make these actions generic enough to be used in common situations. One such
control is the [Dynamic Form Directive](http://transcend.bitbucket.org/#/api/transcend.resource.directive:dynamicForm)
which will automatically create a form based on an object structure - and/or meta-data for that object.

## Installation
The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
commands to install this package:

```
bower install https://bitbucket.org/transcend/bower-resource.git
```

Other options:

*   * Download the code at [http://code.tsstools.com/bower-resource/get/master.zip](http://code.tsstools.com/bower-resource/get/master.zip)
*   * View the repository at [http://code.tsstools.com/bower-resource](http://code.tsstools.com/bower-resource)
*   * If you have permissions, view the source code at [http://code.tsstools.com/web-common](http://code.tsstools.com/web-common/src/ee541a3bce850ae77f6d7948a00cc352b4f67b21/modules/resource/?at=develop)

When you have pulled down the code and included the files (and dependency files), simply add the module as a
dependency to your application:

```
angular.module('myModule', ['transcend.resource']);
```
## To Do
- Add additional unit tests
- Finish documentation